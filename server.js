var express = require('express');  // require express
var app = express();  // create a request handler function
var server = require('http').createServer(app);  // use our app to create a server
var path = require('path');
var ejs =  require('ejs');
var bodyParser =  require('body-parser');
var logger = require("morgan");

 
// on a GET request to default page, do this.... 
app.use(express.static(__dirname+'/assets/'));

app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine



var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
// GETS
// http GET (default and /new-entry)
app.get("/", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/index", function (request, response) {
  response.render("index");
});

// POSTS
// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/index");
});
// 404
app.use(function (request, response) {
  response.status(404).render("404");
});


// on a connection event, act as follows (socket interacts with client)
 
 
// Listen for an app request on port 8081
server.listen(8081, function(){
  console.log('listening on http://127.0.0.1:8081/');
});